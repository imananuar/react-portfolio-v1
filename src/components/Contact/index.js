import emailjs from '@emailjs/browser'
import React, { useEffect, useRef, useState } from 'react'
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet'
import Loader from 'react-loaders'
import AnimatedLetters from '../AnimatedLetters'
import './index.scss' 

function Contact() {

  const [letterClass, setLetterClass] = useState('text-animate');
  const refForm = useRef()  

  useEffect(() => {
    setTimeout(() => {
      setLetterClass("text-animate-hover")
    }, 4000)
  }, [])

  const sendEmail = (e) => {
    e.preventDefault()

    emailjs
      .sendForm(
        'service_yh7i5w4',
        'template_83l5wsc',
        refForm.current,
        'vIf0bDcHInvfOgTYf'
      )

      .then (
        () => {
          alert('Message successfully sent!');
          window.location.reload(false)
        },
        () => {
          alert('Failed to send the message, please try again')
        }
      )
  }

  return (
    <>
      <div className='container contact-page'>
        <div className='text-zone'>
          <h1>
            <AnimatedLetters
              letterClass={letterClass}
              strArray = {['C','o','n','t','a','c','t',' ','M','e']}
              index = {15}
              />
          </h1>
          <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
              Fusce vitae magna non elit mollis blandit sed vitae leo. 
              Curabitur dignissim condimentum metus, a rutrum massa.
          </p>
          <div className='contact-form'>
            <form ref={refForm} onSubmit={sendEmail}>
              <ul>
                <li className='half'>
                  <input type="text" 
                  name="name" 
                  placeholder='Name' 
                  required />  
                </li>
                <li className='half'>
                  <input type="email" 
                  name="email" 
                  placeholder='Email' 
                  required />  
                </li>
                <li>
                  <input placeholder="subject" 
                  type='text' 
                  name='subject' 
                  required />
                </li>
                <li>
                  <textarea placeholder="Message"
                  name='message' 
                  required 
                  ></textarea>
                </li>
                <li>
                  <input type="submit" className='flat-button' value="SEND" />
                </li>
              </ul>
            </form>
          </div>
        </div>
        <div className='info-map'>
          Iman Anuar,
          <br />
          Malaysia,
          <br />
          Jalan Kampung Gajah, <br />
          Pulau Pinang <br />
          <span>imananuar5367@gmail.com</span>
        </div>
        <div className='map-wrap'>
          <MapContainer center={[3.1390, 101.6869]} zoom={13} >
            <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
            <Marker position={[3.1390, 101.6869]}>
              <Popup>Jom Lepak</Popup>
            </Marker>
          </MapContainer>
        </div>
      </div>
      <Loader type='pacman' />
    </>
  )
}

export default Contact