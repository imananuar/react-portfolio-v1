import './index.scss';
import { Link, NavLink } from 'react-router-dom'
import React from 'react'
import LogoS from '../../assets/images/logo-s.png'
import LogoSubtitle from '../../assets/images/logo_sub.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope, faHome, faUser } from '@fortawesome/free-solid-svg-icons'
import { faFacebookSquare, faGithubSquare, faLinkedin, faTwitterSquare } from '@fortawesome/free-brands-svg-icons';

function Sidebar() {
  return (
    <div className='nav-bar'>
       <Link className='logo' to='/'>
         <img src={LogoS} alt='logo'/>
         <img className="sub-logo" src={LogoSubtitle} alt='slobodan'/>
      </Link>
      <nav>

        <NavLink exact="true" activeclassname="active" to="/">
          <FontAwesomeIcon icon={ faHome } color="#4d4d4e" />
        </NavLink>

        <NavLink exact="true" 
        className="about-link" 
        activeclassname="active"
        to="/about">
          <FontAwesomeIcon icon={ faUser } color="#4d4d4e" />
        </NavLink>

        <NavLink exact="true" 
        className="contact-link" 
        activeclassname="active" 
        to="/contact">
          <FontAwesomeIcon icon={ faEnvelope } color="#4d4d4e" />
        </NavLink>
      </nav>
      <ul>
        <li>
          <a target="_blank" rel="noreferrer" href='https://www.linkedin.com/in/imananuar/'>
            <FontAwesomeIcon icon={faLinkedin} color="#4d4d4e" />
          </a>
        </li>
        <li>
          <a target="_blank" rel="noreferrer" href='https://github.com/imananuar'>
            <FontAwesomeIcon icon={faGithubSquare} color="#4d4d4e" />
          </a>
        </li>
        <li>
          <a target="_blank" rel="noreferrer" href='https://www.facebook.com/imananuar9898/'>
            <FontAwesomeIcon icon={faFacebookSquare} color="#4d4d4e" />
          </a>
        </li>
        <li>
          <a target="_blank" rel="noreferrer" href='https://twitter.com/_imananuar'>
            <FontAwesomeIcon icon={faTwitterSquare} color="#4d4d4e" />
          </a>
        </li>
      </ul>
    </div>
  )
}

export default Sidebar